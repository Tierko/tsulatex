del *.gz
del *.log
del *.aux
del *.bbl
del *.out
del *.toc
del *.blg
del *.pdf

pdflatex 10_minimal.tex
pdflatex 20_main-1.tex

pdflatex 30_main-2.tex
pdflatex 30_main-2.tex

pdflatex 40_main-3.tex
pdflatex 40_main-3.tex

pdflatex 50_main-4.tex
bibtex 50_main-4
pdflatex 50_main-4.tex
pdflatex 50_main-4.tex

pdflatex 60_main-5.tex
pdflatex 60_main-5.tex

del *.gz
del *.log
del *.aux
del *.bbl
del *.out
del *.toc
del *.blg
pause